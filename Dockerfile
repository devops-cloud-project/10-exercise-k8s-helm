FROM openjdk:17-alpine
RUN mkdir /opt/app
COPY ./build/libs/bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar /opt/app/
WORKDIR /opt/app
EXPOSE 8080
CMD [ "java", "-jar", "bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar"]

